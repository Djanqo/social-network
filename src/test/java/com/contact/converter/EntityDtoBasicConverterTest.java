package com.contact.converter;

import com.contact.entity.Profile;
import com.contact.entity.Role;
import com.contact.entity.User;
import com.contact.model.ProfileDto;
import com.contact.model.RoleDto;
import com.contact.model.UserDto;
import org.junit.Test;

import java.time.LocalDate;

import static com.contact.converter.EntityDtoBasicConverter.convertBasicProperties;
import static org.junit.Assert.assertEquals;

/**
 * Created by Maxim on 006 06.08.17.
 */
public class EntityDtoBasicConverterTest {

    @Test
    public void correctConvertUserToUserDto() {
        User user = new User();
        user.setId(1L);
        user.setLogin("one");
        user.setPassword("12345678");

        UserDto userDto = EntityDtoBasicConverter.convertBasicProperties(user);
        assertEquals(user.getId(), userDto.getId());
        assertEquals(user.getLogin(), userDto.getLogin());
        assertEquals(user.getPassword(), userDto.getPassword());
    }

    @Test
    public void correctConvertUserDtoToUser() {
        UserDto userDto = new UserDto();
        userDto.setId(2L);
        userDto.setLogin("two");
        userDto.setPassword("87654321");

        User user = EntityDtoBasicConverter.convertBasicProperties(userDto);
        assertEquals(userDto.getId(), user.getId());
        assertEquals(userDto.getLogin(), user.getLogin());
        assertEquals(userDto.getPassword(), user.getPassword());
    }

    @Test
    public void correctConvertProfileToProfileDto() {
        Profile profile = new Profile();
        profile.setId(1L);
        profile.setName("Profile_name");
        profile.setLastName("Profile_last_name");
        profile.setBirthDate(LocalDate.now());

        ProfileDto profileDto = EntityDtoBasicConverter.convertBasicProperties(profile);
        assertEquals(profile.getId(), profileDto.getId());
        assertEquals(profile.getName(), profileDto.getName());
        assertEquals(profile.getLastName(), profileDto.getLastName());
        assertEquals(profile.getBirthDate(), profileDto.getBirthDate());
    }

    @Test
    public void correctConvertProfileDtoToProfile() {
        ProfileDto profileDto = new ProfileDto();
        profileDto.setId(1L);
        profileDto.setName("Profile_name");
        profileDto.setLastName("Profile_last_name");
        profileDto.setBirthDate(LocalDate.now());

        Profile profile = EntityDtoBasicConverter.convertBasicProperties(profileDto);
        assertEquals(profileDto.getId(), profile.getId());
        assertEquals(profileDto.getName(), profile.getName());
        assertEquals(profileDto.getLastName(), profile.getLastName());
        assertEquals(profileDto.getBirthDate(), profile.getBirthDate());
    }

    @Test
    public void correctConvertRoleToRoleDto() {
        Role role = new Role();
        role.setId(1L);
        role.setName("Admin");

        RoleDto roleDto = convertBasicProperties(role);
        assertEquals(role.getId(), roleDto.getId());
        assertEquals(role.getName(), roleDto.getName());
    }

    @Test
    public void correctConvertRoleDtoToRole() {
        RoleDto roleDto = new RoleDto();
        roleDto.setId(2L);
        roleDto.setName("User");

        Role role = EntityDtoBasicConverter.convertBasicProperties(roleDto);
        assertEquals(roleDto.getId(), role.getId());
        assertEquals(roleDto.getName(), role.getName());
    }
}
