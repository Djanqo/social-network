package com.contact.converter;

import com.contact.entity.Profile;
import com.contact.entity.User;
import com.contact.model.UserDto;
import org.junit.Test;

import java.time.LocalDate;

import static com.contact.converter.UserDtoConverter.convertUserToUserDto;
import static org.junit.Assert.assertEquals;

/**
 * Created by Maxim on 007 07.08.17.
 */
public class UserDtoConverterTest {

    @Test
    public void correctConvertUserToUserDto() {
        User user = new User();
        user.setId(1L);
        user.setLogin("one");
        user.setPassword("12345678");

        Profile profile = new Profile();
        profile.setId(1L);
        profile.setName("Profile_name");
        profile.setLastName("Profile_last_name");
        profile.setBirthDate(LocalDate.now());
        user.setProfile(profile);

        UserDto userDto = convertUserToUserDto(user);
        assertEquals(user.getId(), userDto.getId());
        assertEquals(user.getLogin(), userDto.getLogin());
        assertEquals(user.getPassword(), userDto.getPassword());
        assertEquals(user.getProfile().getId(), userDto.getProfileDto().getId());
        assertEquals(user.getProfile().getName(), userDto.getProfileDto().getName());
        assertEquals(user.getProfile().getLastName(), userDto.getProfileDto().getLastName());
        assertEquals(user.getProfile().getBirthDate(), userDto.getProfileDto().getBirthDate());
    }

}
