package com.contact.controller;

import com.contact.model.ProfileDto;
import com.contact.model.SearchRequest;
import com.contact.service.ProfileService;
import com.contact.service.RelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class MainController {

    @Autowired
    private ProfileService profileService;

    @Autowired
    private RelationshipService relationshipService;

    @RequestMapping(value = {"/", "/contact"}, method = RequestMethod.GET)
    public String contact(Model model) {
        ProfileDto profileDto = profileService.getCurrentProfile();
        if (profileDto.getId() == null) {
            profileService.modifyProfile(profileDto);
            profileDto = profileService.getCurrentProfile();
        }
        model.addAttribute("currentProfile", profileDto);
        model.addAttribute("friends", relationshipService.getAllFriends(profileDto));
        model.addAttribute("searchTypes", profileService.getSearchTypes());
        model.addAttribute("friendRequests", relationshipService.getAllFriendRequests(profileDto));
        return "contact";
    }

    @ResponseBody
    @RequestMapping(value = "/modifyProfile", method = RequestMethod.POST)
    public ProfileDto modifyProfile(@RequestBody ProfileDto profileDto) {
        profileService.modifyProfile(profileDto);
        return profileService.getCurrentProfile();
    }

    @ResponseBody
    @RequestMapping(value = "/searchRequest", method = RequestMethod.POST)
    public List<ProfileDto> executeSearchRequest(@RequestBody SearchRequest searchRequest) {
        List<ProfileDto> profileDtos = profileService.executeSearchRequest(searchRequest);
        return profileDtos;
    }

    @ResponseBody
    @RequestMapping(value = "/watchProfile/{id}", method = RequestMethod.POST)
    public ProfileDto watchProfile(@PathVariable Long id) {
        ProfileDto profileDto = profileService.getById(id);
        profileDto = relationshipService.checkFriendStatus(profileDto);
        return profileDto;
    }

    @ResponseBody
    @RequestMapping(value = "/sendFriendRequest/{id}", method = RequestMethod.POST)
    public ProfileDto sendFriendRequest(@PathVariable Long id) {
        ProfileDto profileDto = profileService.getById(id);
        relationshipService.createFriendRequest(profileDto);
        profileDto = relationshipService.checkFriendStatus(profileDto);
        return profileDto;
    }

    @ResponseBody
    @RequestMapping(value = "/acceptFriendRequest/{id}", method = RequestMethod.POST)
    public ProfileDto acceptFriendRequest(@PathVariable Long id) {
        ProfileDto profileDto = profileService.getById(id);
        relationshipService.acceptFriendRequest(profileDto);
        profileDto = relationshipService.checkFriendStatus(profileDto);
        return profileDto;
    }

    @ResponseBody
    @RequestMapping(value = "/declineFriendRequest/{id}", method = RequestMethod.POST)
    public ProfileDto declineFriendRequest(@PathVariable Long id) {
        ProfileDto profileDto = profileService.getById(id);
        relationshipService.declineFriendRequest(profileDto);
        profileDto = relationshipService.checkFriendStatus(profileDto);
        return profileDto;
    }
}
