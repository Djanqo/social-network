package com.contact.controller;

import com.contact.model.ProfileDto;
import com.contact.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ProfileController {

    @Autowired
    ProfileService profileService;

    @RequestMapping(value = "/createProfile", method = RequestMethod.GET)
    public String createProfile() {
        return "createProfile";
    }

    @ResponseBody
    @RequestMapping(value = "/saveProfile", method = RequestMethod.POST)
    public ProfileDto saveProfile(@RequestBody ProfileDto profileDto) {
        profileService.modifyProfile(profileDto);
        return profileDto;
    }
}
