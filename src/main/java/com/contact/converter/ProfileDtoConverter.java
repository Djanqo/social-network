package com.contact.converter;

import com.contact.entity.Profile;
import com.contact.entity.User;
import com.contact.model.ProfileDto;
import com.contact.model.UserDto;

import static com.contact.converter.EntityDtoBasicConverter.convertBasicProperties;

public class ProfileDtoConverter {

    public static ProfileDto convertProfileToProfileDto(Profile profile) {
        ProfileDto profileDto = convertBasicProperties(profile);

        User user = profile.getUser();
        if (user != null) {
            profileDto.setUserDto(convertBasicProperties(user));
        }

        return profileDto;
    }

    public static Profile convertProfileDtoToProfile(ProfileDto profileDto) {
        Profile profile = convertBasicProperties(profileDto);

        UserDto userDto = profileDto.getUserDto();
        if (userDto != null) {
            profile.setUser(convertBasicProperties(userDto));
        }

        return profile;
    }
}
