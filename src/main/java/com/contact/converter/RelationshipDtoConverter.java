package com.contact.converter;

import com.contact.entity.Profile;
import com.contact.entity.Relationship;
import com.contact.model.ProfileDto;
import com.contact.model.RelationshipDto;

import static com.contact.converter.EntityDtoBasicConverter.convertBasicProperties;

public class RelationshipDtoConverter {

    public static RelationshipDto convertRelationshipToRelationshipDto(Relationship relationship) {
        RelationshipDto relationshipDto = convertBasicProperties(relationship);

        Profile profileOne = relationship.getProfileOne();
        if (profileOne != null) {
            relationshipDto.setProfileDtoOne(convertBasicProperties(profileOne));
        }

        Profile profileTwo = relationship.getProfileTwo();
        if (profileTwo != null) {
            relationshipDto.setProfileDtoTwo(convertBasicProperties(profileTwo));
        }

        Profile actionProfile = relationship.getActionProfile();
        if (actionProfile != null) {
            relationshipDto.setActionProfileDto(convertBasicProperties(actionProfile));
        }

        return relationshipDto;
    }

    public static Relationship convertRelationshipDtoToRelationship(RelationshipDto relationshipDto) {
        Relationship relationship = convertBasicProperties(relationshipDto);

        ProfileDto profileDtoOne = relationshipDto.getProfileDtoOne();
        if (profileDtoOne != null) {
            relationship.setProfileOne(convertBasicProperties(profileDtoOne));
        }

        ProfileDto profileDtoTwo = relationshipDto.getProfileDtoTwo();
        if (profileDtoTwo != null) {
            relationship.setProfileTwo(convertBasicProperties(profileDtoTwo));
        }

        ProfileDto actionProfileDto = relationshipDto.getActionProfileDto();
        if (actionProfileDto != null) {
            relationship.setActionProfile(convertBasicProperties(actionProfileDto));
        }

        return relationship;
    }
}
