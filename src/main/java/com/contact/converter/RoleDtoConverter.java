package com.contact.converter;

import com.contact.entity.Role;
import com.contact.entity.User;
import com.contact.model.RoleDto;
import com.contact.model.UserDto;

import java.util.HashSet;
import java.util.Set;

import static com.contact.converter.EntityDtoBasicConverter.convertBasicProperties;

public class RoleDtoConverter {

    public static RoleDto convertRoleToRoleDto(Role role) {
        RoleDto roleDto = convertBasicProperties(role);

        Set<UserDto> userDtos = new HashSet<UserDto>();
        for (User user : role.getUsers()) {
            userDtos.add(convertBasicProperties(user));
        }
        roleDto.setUserDtos(userDtos);

        return roleDto;
    }

    public static Role convertRoleDtoToRole(RoleDto roleDto) {
        Role role = convertBasicProperties(roleDto);

        Set<User> users = new HashSet<User>();
        for (UserDto userDto : roleDto.getUserDtos()) {
            users.add(convertBasicProperties(userDto));
        }
        role.setUsers(users);

        return role;
    }
}
