package com.contact.converter;

import com.contact.entity.Profile;
import com.contact.entity.Role;
import com.contact.entity.User;
import com.contact.model.ProfileDto;
import com.contact.model.RoleDto;
import com.contact.model.UserDto;

import java.util.HashSet;
import java.util.Set;

import static com.contact.converter.EntityDtoBasicConverter.convertBasicProperties;

public class UserDtoConverter {

    public static UserDto convertUserToUserDto(User user) {
        UserDto userDto = convertBasicProperties(user);

        Profile profile = user.getProfile();
        if (profile != null) {
            userDto.setProfileDto(convertBasicProperties(profile));
        }

        Set<RoleDto> roleDtos = new HashSet<RoleDto>();
        for (Role role : user.getRoles()) {
            if (role != null) {
                roleDtos.add(convertBasicProperties(role));
            }
        }
        userDto.setRoleDtos(roleDtos);

        return userDto;
    }

    public static User convertUserDtoToUser(UserDto userDto) {
        User user = convertBasicProperties(userDto);

        ProfileDto profileDto = userDto.getProfileDto();
        if (profileDto != null) {
            user.setProfile(convertBasicProperties(profileDto));
        }

        Set<Role> roles = new HashSet<Role>();
        for (RoleDto roleDto : userDto.getRoleDtos()) {
            if (roleDto != null) {
                roles.add(convertBasicProperties(roleDto));
            }
        }
        user.setRoles(roles);

        return user;
    }
}
