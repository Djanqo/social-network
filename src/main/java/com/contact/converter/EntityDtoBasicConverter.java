package com.contact.converter;

import com.contact.entity.Profile;
import com.contact.entity.Relationship;
import com.contact.entity.Role;
import com.contact.entity.User;
import com.contact.model.ProfileDto;
import com.contact.model.RelationshipDto;
import com.contact.model.RoleDto;
import com.contact.model.UserDto;

public class EntityDtoBasicConverter {
    public static User convertBasicProperties(UserDto userDto) {
        User user = new User();

        user.setId(userDto.getId());

        user.setLogin(userDto.getLogin());

        user.setPassword(userDto.getPassword());

        return user;
    }

    public static UserDto convertBasicProperties(User user) {
        UserDto userDto = new UserDto();

        userDto.setId(user.getId());

        userDto.setPassword(user.getPassword());

        userDto.setLogin(user.getLogin());

        return userDto;
    }

    public static Profile convertBasicProperties(ProfileDto profileDto) {
        Profile profile = new Profile();

        profile.setId(profileDto.getId());

        profile.setName(profileDto.getName());

        profile.setLastName(profileDto.getLastName());

        profile.setBirthDate(profileDto.getBirthDate());

        return profile;
    }

    public static ProfileDto convertBasicProperties(Profile profile) {
        ProfileDto profileDto = new ProfileDto();

        profileDto.setId(profile.getId());

        profileDto.setName(profile.getName());

        profileDto.setLastName(profile.getLastName());

        profileDto.setBirthDate(profile.getBirthDate());

        return profileDto;
    }

    public static Role convertBasicProperties(RoleDto roleDto) {
        Role role = new Role();

        role.setId(roleDto.getId());

        role.setName(roleDto.getName());

        return role;
    }

    public static RoleDto convertBasicProperties(Role role) {
        RoleDto roleDto = new RoleDto();

        roleDto.setId(role.getId());

        roleDto.setName(role.getName());

        return roleDto;
    }

    public static Relationship convertBasicProperties(RelationshipDto relationshipDto) {
        Relationship relationship = new Relationship();

        relationship.setId(relationshipDto.getId());

        relationship.setStatus(relationshipDto.getStatus());

        return relationship;
    }

    public static RelationshipDto convertBasicProperties(Relationship relationship) {
        RelationshipDto relationshipDto = new RelationshipDto();

        relationshipDto.setId(relationship.getId());

        relationshipDto.setStatus(relationship.getStatus());

        return relationshipDto;
    }
}
