package com.contact.entity;

import javax.persistence.*;

@Entity
@Table(name = "relationship",
        uniqueConstraints = @UniqueConstraint(columnNames = {"profile_one_id", "profile_two_id"}))
public class Relationship {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "profile_one_id", nullable = false)
    private Profile profileOne;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "profile_two_id", nullable = false)
    private Profile profileTwo;

    @Column(name = "status", nullable = false)
    private Byte status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "action_profile_id", nullable = false)
    private Profile actionProfile;

    public Relationship() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Profile getProfileOne() {
        return profileOne;
    }

    public void setProfileOne(Profile profileOne) {
        this.profileOne = profileOne;
    }

    public Profile getProfileTwo() {
        return profileTwo;
    }

    public void setProfileTwo(Profile profileTwo) {
        this.profileTwo = profileTwo;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Profile getActionProfile() {
        return actionProfile;
    }

    public void setActionProfile(Profile actionProfile) {
        this.actionProfile = actionProfile;
    }
}
