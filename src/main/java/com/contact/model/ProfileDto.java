package com.contact.model;

import java.time.LocalDate;

public class ProfileDto {

    private Long id;

    private String name;

    private String lastName;

    private LocalDate birthDate;

    private UserDto userDto;

    private Byte friendStatus;

    public ProfileDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public Byte getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(Byte friendStatus) {
        this.friendStatus = friendStatus;
    }
}
