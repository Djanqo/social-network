package com.contact.model;

public class SearchRequest {

    private SearchType type;

    private String searchingValue;

    public SearchType getType() {
        return type;
    }

    public void setType(SearchType type) {
        this.type = type;
    }

    public String getSearchingValue() {
        return searchingValue;
    }

    public void setSearchingValue(String searchingValue) {
        this.searchingValue = searchingValue;
    }
}
