package com.contact.model;

import java.util.Arrays;
import java.util.List;

public enum SearchType {

    Name, Surname;

    public static List<String> getAllTypes() {
        return Arrays.asList(Name.name(), Surname.name());
    }
}
