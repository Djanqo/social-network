package com.contact.model;

public class RelationshipDto {

    private Long id;

    private ProfileDto profileDtoOne;

    private ProfileDto profileDtoTwo;

    private Byte status;

    private ProfileDto actionProfileDto;

    public RelationshipDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProfileDto getProfileDtoOne() {
        return profileDtoOne;
    }

    public void setProfileDtoOne(ProfileDto profileDtoOne) {
        this.profileDtoOne = profileDtoOne;
    }

    public ProfileDto getProfileDtoTwo() {
        return profileDtoTwo;
    }

    public void setProfileDtoTwo(ProfileDto profileDtoTwo) {
        this.profileDtoTwo = profileDtoTwo;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public ProfileDto getActionProfileDto() {
        return actionProfileDto;
    }

    public void setActionProfileDto(ProfileDto actionProfileDto) {
        this.actionProfileDto = actionProfileDto;
    }
}
