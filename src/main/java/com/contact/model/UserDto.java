package com.contact.model;

import java.util.HashSet;
import java.util.Set;

public class UserDto {

    private Long id;

    private String login;

    private String password;

    private String confirmPassword;

    private ProfileDto profileDto;

    /*private Set<GroupDto> groupDtos = new HashSet<GroupDto>();*/

    private Set<RoleDto> roleDtos = new HashSet<RoleDto>();

    public UserDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public ProfileDto getProfileDto() {
        return profileDto;
    }

    public void setProfileDto(ProfileDto profileDto) {
        this.profileDto = profileDto;
    }

    /*public Set<GroupDto> getGroupDtos() {
        return groupDtos;
    }

    public void setGroupDtos(Set<GroupDto> groupDtos) {
        this.groupDtos = groupDtos;
    }*/

    public Set<RoleDto> getRoleDtos() {
        return roleDtos;
    }

    public void setRoleDtos(Set<RoleDto> roleDtos) {
        this.roleDtos = roleDtos;
    }
}
