package com.contact.model;

import java.util.HashSet;
import java.util.Set;

public class RoleDto {

    private Long id;

    private String name;

    private Set<UserDto> userDtos = new HashSet<UserDto>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserDto> getUserDtos() {
        return userDtos;
    }

    public void setUserDtos(Set<UserDto> userDtos) {
        this.userDtos = userDtos;
    }
}
