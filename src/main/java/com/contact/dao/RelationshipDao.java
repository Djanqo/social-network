package com.contact.dao;

import com.contact.model.ProfileDto;
import com.contact.model.RelationshipDto;

import java.util.List;

public interface RelationshipDao {

    void save(RelationshipDto relationshipDto);

    void update(ProfileDto profileDtoOne, ProfileDto profileDtoTwo, Byte status, ProfileDto actionProfileDto);

    List<RelationshipDto> findAllFriendship(ProfileDto currentProfileDto);

    List<RelationshipDto> findAllFriendRequests(ProfileDto currentProfileDto);

    RelationshipDto findFriendship(ProfileDto profileDtoOne, ProfileDto profileDtoTwo);
}
