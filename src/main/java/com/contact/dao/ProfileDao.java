package com.contact.dao;

import com.contact.model.ProfileDto;

import java.util.List;

public interface ProfileDao {

    ProfileDto findById(Long id);

    ProfileDto findByLogin(String login);

    List<ProfileDto> findByName(String name);

    List<ProfileDto> findByLastName(String lastName);

    void saveOrUpdate(ProfileDto profileDto);
}
