package com.contact.dao.impl;

import com.contact.dao.RelationshipDao;
import com.contact.entity.Relationship;
import com.contact.model.ProfileDto;
import com.contact.model.RelationshipDto;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.contact.converter.ProfileDtoConverter.convertProfileDtoToProfile;
import static com.contact.converter.RelationshipDtoConverter.convertRelationshipDtoToRelationship;
import static com.contact.converter.RelationshipDtoConverter.convertRelationshipToRelationshipDto;

@Repository
public class RelationshipDaoImpl implements RelationshipDao {

    private final static byte PENDING = 0;
    private final static byte ACCEPTED = 1;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void save(RelationshipDto relationshipDto) {
        Relationship relationship = convertRelationshipDtoToRelationship(relationshipDto);
        sessionFactory.getCurrentSession().save(relationship);
    }

    @Override
    @Transactional
    public void update(ProfileDto profileDtoOne, ProfileDto profileDtoTwo, Byte status, ProfileDto actionProfileDto) {
        String hqlUpdate =
                "update Relationship r " +
                        "set r.status = :newStatus, r.actionProfile = :newActionProfile " +
                        "where r.profileOne = :profileOne and r.profileTwo = :profileTwo";
        sessionFactory.getCurrentSession().createQuery(hqlUpdate)
                .setParameter("newStatus", status)
                .setParameter("newActionProfile", convertProfileDtoToProfile(actionProfileDto))
                .setParameter("profileOne", convertProfileDtoToProfile(profileDtoOne))
                .setParameter("profileTwo", convertProfileDtoToProfile(profileDtoTwo))
                .executeUpdate();
    }

    @Override
    @Transactional(readOnly = true)
    public List<RelationshipDto> findAllFriendship(ProfileDto currentProfileDto) {
        String hqlSelect =
                "select r from Relationship r " +
                        "where (r.profileOne = :current or r.profileTwo = :current) and r.status = :status";
        List<Relationship> relationships = relationships = sessionFactory.getCurrentSession().createQuery(hqlSelect)
                .setParameter("current", convertProfileDtoToProfile(currentProfileDto))
                .setParameter("status", ACCEPTED)
                .list();

        List<RelationshipDto> relationshipDtos = new ArrayList<RelationshipDto>();
        for (Relationship relationship : relationships) {
            relationshipDtos.add(convertRelationshipToRelationshipDto(relationship));
        }
        return relationshipDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<RelationshipDto> findAllFriendRequests(ProfileDto currentProfileDto) {
        String hqlSelect =
                "select r from Relationship r " +
                        "where (r.profileOne = :current or r.profileTwo = :current) " +
                        "and r.status = :status and r.actionProfile != :current";
        List<Relationship> relationships = sessionFactory.getCurrentSession().createQuery(hqlSelect)
                .setParameter("current", convertProfileDtoToProfile(currentProfileDto))
                .setParameter("status", PENDING)
                .list();
        List<RelationshipDto> relationshipDtos = new ArrayList<RelationshipDto>();
        for (Relationship relationship : relationships) {
            relationshipDtos.add(convertRelationshipToRelationshipDto(relationship));
        }

        return relationshipDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public RelationshipDto findFriendship(ProfileDto profileDtoOne, ProfileDto profileDtoTwo) {
        String hqlSelect =
                "select r from Relationship r " +
                        "where r.profileOne = :profileOne and r.profileTwo = :profileTwo";
        List<Relationship> relationships = sessionFactory.getCurrentSession().createQuery(hqlSelect)
                .setParameter("profileOne", convertProfileDtoToProfile(profileDtoOne))
                .setParameter("profileTwo", convertProfileDtoToProfile(profileDtoTwo))
                .list();

        if (relationships.isEmpty()) {
            return null;
        } else {
            return convertRelationshipToRelationshipDto(relationships.get(0));
        }
    }
}
