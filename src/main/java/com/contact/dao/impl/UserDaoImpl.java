package com.contact.dao.impl;

import com.contact.dao.UserDao;
import com.contact.entity.User;
import com.contact.model.UserDto;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.contact.converter.UserDtoConverter.convertUserDtoToUser;
import static com.contact.converter.UserDtoConverter.convertUserToUserDto;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void save(UserDto userDto) {
        User user = convertUserDtoToUser(userDto);
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserDto> findAll() {
        List<User> users = sessionFactory.getCurrentSession().createQuery("from User").list();
        List<UserDto> result = new ArrayList<UserDto>(users.size());
        for (User user : users) {
            result.add(convertUserToUserDto(user));
        }
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDto findById(Long id) {
        return convertUserToUserDto((User) sessionFactory.getCurrentSession().load(User.class, id));
    }

    @Override
    @Transactional(readOnly = true)
    public UserDto findByLogin(String login) {
        String hqlSelect =
                "select i from User i " +
                        "where i.login = :login";
        List<User> users = sessionFactory.getCurrentSession()
                .createQuery(hqlSelect)
                .setParameter("login", login)
                .list();
        if (users.isEmpty()) {
            return null;
        } else {
            return convertUserToUserDto(users.get(0));
        }
    }

    @Override
    @Transactional
    public void delete(Long id) {
        User user = (User) sessionFactory.getCurrentSession().load(User.class, id);
        if (user != null) {
            sessionFactory.getCurrentSession().delete(user);
        }
    }
}
