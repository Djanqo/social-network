package com.contact.dao.impl;

import com.contact.dao.RoleDao;
import com.contact.entity.Role;
import com.contact.model.RoleDto;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import static com.contact.converter.RoleDtoConverter.convertRoleToRoleDto;

@Repository
public class RoleDaoImpl implements RoleDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(readOnly = true)
    public RoleDto findById(Long id) {
        return convertRoleToRoleDto((Role) sessionFactory.getCurrentSession().load(Role.class, id));
    }
}
