package com.contact.dao.impl;

import com.contact.dao.ProfileDao;
import com.contact.entity.Profile;
import com.contact.model.ProfileDto;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.contact.converter.ProfileDtoConverter.convertProfileDtoToProfile;
import static com.contact.converter.ProfileDtoConverter.convertProfileToProfileDto;

@Repository
public class ProfileDaoImpl implements ProfileDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(readOnly = true)
    public ProfileDto findById(Long id) {
        return convertProfileToProfileDto((Profile) sessionFactory.getCurrentSession().load(Profile.class, id));
    }

    @Override
    @Transactional(readOnly = true)
    public ProfileDto findByLogin(String login) {
        String hqlSelect =
                "select p from Profile p " +
                        "join p.user user " +
                        "where user.login = :login";
        List<Profile> profiles = sessionFactory.getCurrentSession().createQuery(hqlSelect)
                .setParameter("login", login)
                .list();
        if (profiles.isEmpty()) {
            return null;
        } else {
            return convertProfileToProfileDto(profiles.get(0));
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProfileDto> findByName(String name) {
        String hqlSelect =
                "select p from Profile p " +
                        "where p.name = :name";
        List<Profile> profiles = sessionFactory.getCurrentSession()
                .createQuery(hqlSelect)
                .setParameter("name", name)
                .list();

        List<ProfileDto> profileDtos = new ArrayList<ProfileDto>(profiles.size());
        for (Profile profile : profiles) {
            profileDtos.add(convertProfileToProfileDto(profile));
        }
        return profileDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProfileDto> findByLastName(String lastName) {
        String hqlSelect =
                "select p from Profile p " +
                        "where p.lastName = :lastName";
        List<Profile> profiles = sessionFactory.getCurrentSession()
                .createQuery(hqlSelect)
                .setParameter("lastName", lastName)
                .list();

        List<ProfileDto> profileDtos = new ArrayList<ProfileDto>(profiles.size());
        for (Profile profile : profiles) {
            profileDtos.add(convertProfileToProfileDto(profile));
        }
        return profileDtos;
    }

    @Override
    @Transactional
    public void saveOrUpdate(ProfileDto profileDto) {
        Profile profile = convertProfileDtoToProfile(profileDto);
        sessionFactory.getCurrentSession().saveOrUpdate(profile);
    }
}
