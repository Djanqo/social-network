package com.contact.dao;

import com.contact.model.UserDto;

import java.util.List;

public interface UserDao {

    void save(UserDto userDto);

    List<UserDto> findAll();

    UserDto findById(Long id);

    UserDto findByLogin(String login);

    void delete(Long id);

}
