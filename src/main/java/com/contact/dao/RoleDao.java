package com.contact.dao;

import com.contact.model.RoleDto;

public interface RoleDao {

    RoleDto findById(Long id);

}
