package com.contact.service.impl;

import com.contact.dao.RoleDao;
import com.contact.dao.UserDao;
import com.contact.model.ProfileDto;
import com.contact.model.RoleDto;
import com.contact.model.UserDto;
import com.contact.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    public final static Long ROLE_USER = 1L;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void createUser(UserDto userDto) {
        userDto.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        Set<RoleDto> roleDtos = new HashSet<>();
        roleDtos.add(roleDao.findById(ROLE_USER));
        userDto.setRoleDtos(roleDtos);
        userDto.setProfileDto(new ProfileDto());
        userDao.save(userDto);
    }

    @Override
    public List<UserDto> getAllUsers() {
        return userDao.findAll();
    }

    @Override
    public UserDto getByLogin(String login) {
        return userDao.findByLogin(login);
    }

    @Override
    public String getCurrentLogin() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }

    @Override
    public Long getCurrentId() {
        UserDto userDto = userDao.findByLogin(getCurrentLogin());
        return userDto.getId();
    }

}
