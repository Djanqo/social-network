package com.contact.service.impl;

import com.contact.dao.RelationshipDao;
import com.contact.model.ProfileDto;
import com.contact.model.RelationshipDto;
import com.contact.service.ProfileService;
import com.contact.service.RelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RelationshipServiceImpl implements RelationshipService {

    private final static byte PENDING = 0;
    private final static byte ACCEPTED = 1;
    private final static byte DECLINED = 2;

    private final static byte PRETENDER = 0;
    private final static byte WAIT_FOR_ANSWER = 1;
    private final static byte FRIEND = 2;
    private final static byte NOBODY = 3;
    private final static byte IGNORED = 4;


    @Autowired
    private RelationshipDao relationshipDao;

    @Autowired
    private ProfileService profileService;

    @Override
    public boolean currentProfileDtoIdisLess(ProfileDto currentProfileDto, ProfileDto potentialFriendProfileDto) {
        if (currentProfileDto.getId() < potentialFriendProfileDto.getId()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void createFriendRequest(ProfileDto potentialFriendProfileDto) {
        RelationshipDto relationshipDto = new RelationshipDto();
        ProfileDto currentProfileDto = profileService.getCurrentProfile();

        if (currentProfileDtoIdisLess(currentProfileDto, potentialFriendProfileDto)) {
            relationshipDto.setProfileDtoOne(currentProfileDto);
            relationshipDto.setProfileDtoTwo(potentialFriendProfileDto);
        } else {
            relationshipDto.setProfileDtoOne(potentialFriendProfileDto);
            relationshipDto.setProfileDtoTwo(currentProfileDto);
        }
        relationshipDto.setStatus(PENDING);

        relationshipDto.setActionProfileDto(currentProfileDto);

        relationshipDao.save(relationshipDto);
    }

    @Override
    public void processingFriendRequest(ProfileDto potentialFriendProfileDto, Byte status) {
        ProfileDto currentProfileDto = profileService.getCurrentProfile();
        if (currentProfileDtoIdisLess(currentProfileDto, potentialFriendProfileDto)) {
            relationshipDao.update(currentProfileDto, potentialFriendProfileDto, status, currentProfileDto);
        } else {
            relationshipDao.update(potentialFriendProfileDto, currentProfileDto, status, currentProfileDto);
        }
    }

    @Override
    public void acceptFriendRequest(ProfileDto potentialFriendProfileDto) {
        processingFriendRequest(potentialFriendProfileDto, ACCEPTED);
    }

    @Override
    public void declineFriendRequest(ProfileDto potentialFriendProfileDto) {
        processingFriendRequest(potentialFriendProfileDto, DECLINED);
    }

    @Override
    public List<ProfileDto> getAllFriends(ProfileDto currentProfileDto) {
        List<RelationshipDto> relationshipDtos = relationshipDao.findAllFriendship(currentProfileDto);

        List<ProfileDto> profileDtos = new ArrayList<ProfileDto>();
        for (RelationshipDto relationshipDto : relationshipDtos) {
            if (relationshipDto.getProfileDtoOne().getId() == currentProfileDto.getId()) {
                profileDtos.add(relationshipDto.getProfileDtoTwo());
            } else {
                profileDtos.add(relationshipDto.getProfileDtoOne());
            }
        }
        return profileDtos;
    }

    @Override
    public List<ProfileDto> getAllFriendRequests(ProfileDto currentProfileDto) {
        List<RelationshipDto> relationshipDtos = relationshipDao.findAllFriendRequests(currentProfileDto);

        List<ProfileDto> profileDtos = new ArrayList<ProfileDto>();
        for (RelationshipDto relationshipDto : relationshipDtos) {
            if (relationshipDto.getProfileDtoOne().getId() == currentProfileDto.getId()) {
                profileDtos.add(relationshipDto.getProfileDtoTwo());
            } else {
                profileDtos.add(relationshipDto.getProfileDtoOne());
            }
        }
        return profileDtos;
    }

    @Override
    public ProfileDto checkFriendStatus(ProfileDto watchingProfileDto) {
        ProfileDto currentProfileDto = profileService.getCurrentProfile();
        RelationshipDto relationshipDto = new RelationshipDto();
        if (currentProfileDtoIdisLess(currentProfileDto, watchingProfileDto)) {
            relationshipDto = relationshipDao.findFriendship(currentProfileDto, watchingProfileDto);
        } else {
            relationshipDto = relationshipDao.findFriendship(watchingProfileDto, currentProfileDto);
        }
        if (relationshipDto == null) {
            watchingProfileDto.setFriendStatus(NOBODY);
            return watchingProfileDto;
        } else {
            switch (relationshipDto.getStatus()) {
                case PENDING:
                    if (relationshipDto.getActionProfileDto().getId() == currentProfileDto.getId()) {
                        watchingProfileDto.setFriendStatus(PRETENDER);
                    } else {
                        watchingProfileDto.setFriendStatus(WAIT_FOR_ANSWER);
                    }
                    break;
                case ACCEPTED:
                    watchingProfileDto.setFriendStatus(FRIEND);
                    break;
                case DECLINED:
                    watchingProfileDto.setFriendStatus(IGNORED);
                    break;
            }
        }
        return watchingProfileDto;
    }
}
