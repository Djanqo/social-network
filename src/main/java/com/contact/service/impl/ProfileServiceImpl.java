package com.contact.service.impl;

import com.contact.dao.ProfileDao;
import com.contact.dao.UserDao;
import com.contact.model.ProfileDto;
import com.contact.model.SearchRequest;
import com.contact.model.SearchType;
import com.contact.model.UserDto;
import com.contact.service.ProfileService;
import com.contact.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileServiceImpl implements ProfileService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ProfileDao profileDao;

    @Autowired
    private UserService userService;

    @Override
    public ProfileDto getById(Long id) {
        return profileDao.findById(id);
    }

    @Override
    public ProfileDto getCurrentProfile() {
        ProfileDto profileDto = profileDao.findByLogin(userService.getCurrentLogin());
        if (profileDto != null) {
            return profileDto;
        } else {
            return new ProfileDto();
        }
    }

    @Override
    public void modifyProfile(ProfileDto profileDto) {
        UserDto userDto = userDao.findByLogin(userService.getCurrentLogin());
        profileDto.setId(userDto.getId());
        profileDao.saveOrUpdate(profileDto);
    }

    @Override
    public List<String> getSearchTypes() {
        return SearchType.getAllTypes();
    }

    @Override
    public List<ProfileDto> searchByName(String name) {
        return profileDao.findByName(name);
    }

    @Override
    public List<ProfileDto> searchByLastName(String lastName) {
        return profileDao.findByLastName(lastName);
    }

    @Override
    public List<ProfileDto> executeSearchRequest(SearchRequest searchRequest) {
        switch (searchRequest.getType()) {
            case Name:
                return profileDao.findByName(searchRequest.getSearchingValue());
            case Surname:
                return profileDao.findByLastName(searchRequest.getSearchingValue());
            default:
                return null;
        }
    }
}
