package com.contact.service.impl;

import com.contact.dao.UserDao;
import com.contact.model.RoleDto;
import com.contact.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        UserDto userDto = userDao.findByLogin(login);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

        for (RoleDto roleDto : userDto.getRoleDtos()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(roleDto.getName()));
        }
        return new org.springframework.security.core.userdetails
                .User(userDto.getLogin(), userDto.getPassword(), grantedAuthorities);
    }
}
