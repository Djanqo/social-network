package com.contact.service;

import com.contact.model.ProfileDto;

import java.util.List;

public interface RelationshipService {

    public boolean currentProfileDtoIdisLess(ProfileDto currentProfileDto, ProfileDto potentialFriendProfileDto);

    public void createFriendRequest(ProfileDto potentialFriendProfileDto);

    public void processingFriendRequest(ProfileDto potentialFriendProfileDto, Byte status);

    public void acceptFriendRequest(ProfileDto potentialFriendProfileDto);

    public void declineFriendRequest(ProfileDto potentialFriendProfileDto);

    public List<ProfileDto> getAllFriends(ProfileDto currentProfileDto);

    public List<ProfileDto> getAllFriendRequests(ProfileDto currentProfileDto);

    public ProfileDto checkFriendStatus(ProfileDto watchingProfileDto);
}
