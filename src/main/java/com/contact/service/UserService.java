package com.contact.service;

import com.contact.model.UserDto;

import java.util.List;

public interface UserService {

    public void createUser(UserDto userDto);

    public List<UserDto> getAllUsers();

    public UserDto getByLogin(String login);

    public String getCurrentLogin();

    public Long getCurrentId();
}
