package com.contact.service;

public interface SecurityService {

    String findLoggedInUserLogin();

    void autoLogin(String login, String password);

}
