package com.contact.service;

import com.contact.model.ProfileDto;
import com.contact.model.SearchRequest;

import java.util.List;

public interface ProfileService {

    ProfileDto getById(Long id);

    ProfileDto getCurrentProfile();

    void modifyProfile(ProfileDto profileDto);

    List<String> getSearchTypes();

    List<ProfileDto> searchByName(String name);

    List<ProfileDto> searchByLastName(String lastName);

    List<ProfileDto> executeSearchRequest(SearchRequest searchRequest);
}
