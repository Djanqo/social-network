<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>

    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/core/css/registration.css">
</head>
<body>
<div class="container">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
        <form:form method="POST" modelAttribute="userForm" class="form-signin">
            <div class="navbar-brand" align="center">
                CONTACT
            </div>
            <h2 class="form-signin-heading">Create your account</h2>
            <spring:bind path="login">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label for="login-create">Login</label>
                    <form:input type="text" path="login" class="form-control" id="login-create"
                                placeholder="min 6 characters" autofocus="true"></form:input>
                    <form:errors path="login"></form:errors>
                </div>
            </spring:bind>

            <spring:bind path="password">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label for="password-create">Password</label>
                    <form:input type="password" path="password" class="form-control"
                                id="password-create" placeholder="min 8 characters"></form:input>
                    <form:errors path="password"></form:errors>
                </div>
            </spring:bind>

            <spring:bind path="confirmPassword">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label for="confirmpass-create">Confirm password</label>
                    <form:input type="password" path="confirmPassword" class="form-control"
                                id="confirmpass-create" placeholder="Confirm your password"></form:input>
                    <form:errors path="confirmPassword"></form:errors>
                </div>
            </spring:bind>

            <button class="btn btn-lg btn-default btn-block" type="submit">Create</button>
        </form:form>
    </div>
</div>

<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/resources/core/js/registration.js"></script>
</body>
</html>
