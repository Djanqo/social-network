<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Log in with your account</title>

    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/core/css/login.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <form method="POST" action="/login" class="form-signin">
                <div class="navbar-brand" align="center">
                    CONTACT
                </div>
                <h2 class="form-signin-heading">Sign in to continue</h2>
                <div class="form-group ${error != null ? 'has-error' : ''}">
                    <span>${message}</span>
                    <input name="login" type="text" class="form-control" placeholder="Login" required autofocus>
                    <input name="password" type="password" class="form-control" placeholder="Password" required>
                    <span>${error}</span>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <button class="btn btn-lg btn-default btn-block" type="submit" id="submit-login">
                        Sign in
                    </button>
                    <h5 class="text-right"><a data-toggle="modal" href="/registration">Create an account</a></h5>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/resources/core/js/login.js"></script>
</body>
</html>
