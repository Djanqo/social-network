<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="currentProfile" scope="request" type="com.contact.model.ProfileDto"/>
<jsp:useBean id="friends" scope="request" type="java.util.List"/>
<jsp:useBean id="searchTypes" scope="request" type="java.util.List"/>
<jsp:useBean id="friendRequests" scope="request" type="java.util.List"/>

<html>
<head>
    <title>Contact</title>

    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/core/css/contact.css">

    <meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
</head>
<body>
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="/contact" class="navbar-brand">CONTACT</a>
        </div>
        <ul class="nav navbar-nav">
            <li role="presentation" class="active"><a href="#" id="profile-nav">Profile</a></li>
            <li role="presentation"><a href="#" id="friends-nav">Friends</a></li>
            <li role="presentation"><a href="#" id="search-nav">Search</a></li>
        </ul>
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <form class="navbar-form navbar-right" id="logoutForm" method="POST" action="${contextPath}/logout">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <span>${pageContext.request.userPrincipal.name} | </span>
                <a href="#" onclick="document.forms['logoutForm'].submit()">Logout</a>
            </form>
        </c:if>
    </div>
</nav>
<div class="top-buffer tab-panel" id="profile-panel">
    <div class="container">
        <div class="row">
            <div class="img-thumbnail col-lg-offset-2 col-lg-2"></div>
            <div class="col-lg-6">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <td>Name:</td>
                        <td id="nameCurrent">${currentProfile.name}</td>
                    </tr>
                    <tr>
                        <td>Last name:</td>
                        <td id="lastNameCurrent">${currentProfile.lastName}</td>
                    </tr>
                    <tr>
                        <td>Birth date:</td>
                        <td id="birthDateCurrent">${currentProfile.birthDate}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row col-lg-offset-9 col-lg-1">
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modifyModal">
                Modify
            </button>

            <div class="modal fade" id="modifyModal" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Please, input/modify your data</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name"
                                           value="${currentProfile.name}"/>
                                </div>
                                <div class="form-group">
                                    <label for="lastName">Last name</label>
                                    <input type="text" class="form-control" id="lastName"
                                           value="${currentProfile.lastName}"/>
                                </div>
                                <div class="form-group">
                                    <label for="birthDate">Birth date</label>
                                    <div><input type="date" class="dateInput" id="birthDate"
                                                value="${currentProfile.birthDate}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="modify-profile">
                                Apply
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="top-buffer tab-panel hidden" id="friends-panel">
    <div class="row top-buffer">
        <div class="col-lg-offset-3 col-lg-6">
            <div><h4 id="friendListTitle">The friend list:</h4></div>
            <div class="list-group" id="friend-list">
                <c:forEach items="${friends}" var="friend">
                    <a href="#" class="list-group-item" data-toggle="modal"
                       data-target="#profileModal" id="${friend.id}">
                            ${friend.name} ${friend.lastName}
                    </a>
                </c:forEach>
            </div>
            <div><h4 id="friendRequestTitle">The list of friend requests:</h4></div>
            <div class="list-group" id="friend-request-list">
                <c:forEach items="${friendRequests}" var="friendRequest">
                    <a href="#" class="list-group-item" data-toggle="modal"
                       data-target="#profileModal" id="${friendRequest.id}">
                            ${friendRequest.name} ${friendRequest.lastName}
                    </a>
                </c:forEach>
            </div>
            <div>
                <button type="button" class="btn btn-default" id="refresh">
                    Refresh
                </button>
            </div>
        </div>
    </div>
</div>
<div class="top-buffer tab-panel hidden" id="search-panel">
    <div class="row top-buffer">
        <div class="col-lg-offset-3 col-lg-6">
            <div class="form-inline">
                <div class="form-group">
                    <input type="text" class="form-control" id="searching-value">
                </div>
                <div class="form-group">
                    <select class="form-control" id="typeSelect">
                        <c:forEach items="${searchTypes}" var="type">
                            <option>${type}</option>
                        </c:forEach>
                    </select>
                </div>
                <button class="btn btn-default" type="submit" id="search-submit">Search profile</button>
            </div>
        </div>
    </div>
    <div class="row top-buffer">
        <div class="col-lg-offset-3 col-lg-6">
            <div class="list-group" id="result-list">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="profileModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a class="hidden" id="watchingId"></a>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <%--<div class="img-thumbnail"></div>--%>
                <table class="table table-hover table-bordered">
                    <tbody>
                    <tr>
                        <td>Name:</td>
                        <td id="nameWatching"></td>
                    </tr>
                    <tr>
                        <td>Last name:</td>
                        <td id="lastNameWatching"></td>
                    </tr>
                    <tr>
                        <td>Birth date:</td>
                        <td id="birthDateWatching"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default hidden" id="send">Send friend request</button>
                <button type="button" class="btn btn-default hidden" id="accept">Accept friend request</button>
                <button type="button" class="btn btn-default hidden" id="decline">Decline friend request</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/resources/core/js/contact.js"></script>
</body>
</html>
