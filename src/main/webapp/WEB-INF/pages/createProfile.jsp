<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create profile</title>
    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/core/css/createProfile.css">

    <meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
</head>
<body>
<div class="container">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
        <div class="form-signin">
            <div class="navbar-brand" align="center">
                CONTACT
            </div>
            <h2 class="form-signin-heading">Create your profile</h2>
            <div class="form-group">
                <label for="name-create">Name:</label>
                <input type="text" class="form-control" id="name-create"
                       placeholder="Input your name" autofocus="true"/>
            </div>
            <div class="form-group">
                <label for="lastName-create">Last name:</label>
                <input type="text" class="form-control"
                       id="lastName-create" placeholder="Input your surname"/>
            </div>
            <div class="form-group">
                <label for="birthDate-create">Birth date:</label>
                <div>
                    <input type="date" path="birthDate" class="dateInput" id="birthDate-create"/>
                </div>
            </div>

            <button class="btn btn-lg btn-default btn-block" type="submit" id="create-profile">Create</button>
        </div>
    </div>
</div>

<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/resources/core/js/createProfile.js"></script>
</body>
</html>
