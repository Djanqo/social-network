$(function () {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
});

$(document).ready(function () {
    var switcher = function (name) {
        $(".tab-panel").addClass("hidden");
        $("#" + name + "-panel").removeClass("hidden");

        $("ul.navbar-nav li").removeClass("active");
        $("#" + name + "-nav").parent().addClass("active");
    };

    $("#profile-nav").click(function () {
        switcher("profile");
    });

    $("#friends-nav").click(function () {
        switcher("friends");
    });

    $("#search-nav").click(function () {
        switcher("search");
    });

    var updateProfileData = function (profile) {
        $("#nameCurrent").html(profile.name);
        $("#lastNameCurrent").html(profile.lastName);
        $("#birthDateCurrent").html(profile.birthDate);
    };

    $("#modify-profile").click(function () {
        var name = $("#name").val();
        var last = $("#lastName").val();
        var birth = $("#birthDate").val();
        $.ajax({
            type: "POST",
            url: "/modifyProfile",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{"name": "' + name + '", "lastName": "' + last + '", "birthDate": "' + birth + '"}',
            success: function (data) {
                /*updateProfileData(data);*/
                /*window.location.href = "/contact";*/
                $("#nameCurrent").html(name);
                $("#lastNameCurrent").html(last);
                $("#birthDateCurrent").html(birth);
            }
        });
    });

    var updateResultList = function (profiles) {
        $("#result-list > a").remove();
        $.each(profiles, function (key, profile) {
            var htmlrow = '<a href="#" class="list-group-item" data-toggle="modal" data-target="#profileModal" id="' +
                profile.id + '">' + profile.name + ' ' + profile.lastName + '</a>';
            $("#result-list").append(htmlrow);
        })
    };

    $("#search-submit").click(function () {
        var type = $("#typeSelect option:selected").text();
        var searchingValue = $("#searching-value").val();
        $.ajax({
            type: "POST",
            url: "/searchRequest",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{"type": "' + type + '", "searchingValue": "' + searchingValue + '"}',
            success: function (data) {
                updateResultList(data);
            }
        });
    });

    var updateWatchingProfile = function (profile) {
        $("#watchingId").text(profile.id);
        $("#nameWatching").text(profile.name);
        $("#lastNameWatching").text(profile.lastName);
        $("#birthDateWatching").text(profile.birthDate);
    };

    var watcher = function (id) {
        $("#watchingId").empty();
        $("#nameWatching").empty();
        $("#lastNameWatching").empty();
        $("#birthDateWatching").empty();
        $.ajax({
            type: "POST",
            url: "/watchProfile/" + id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                updateWatchingProfile(data);
            }
        });
    };

    var refreshButtons = function () {
        $("#send").addClass("hidden");
        $("#accept").addClass("hidden");
        $("#decline").addClass("hidden");
        $("#send").removeAttr("disabled");
    };

    $("#friend-list").click(function (event) {
        refreshButtons();
        var id = event.target.id;
        watcher(id);
    });

    $("#friend-request-list").click(function (event) {
        refreshButtons()

        $("#accept").removeClass("hidden");
        $("#decline").removeClass("hidden");

        var id = event.target.id;
        watcher(id);
    });

    $('#refresh').click(function () {
        location.reload();
    });

    $("#result-list").click(function (event) {
        refreshButtons()
        var id = event.target.id;
        $("#watchingId").empty();
        $("#nameWatching").empty();
        $("#lastNameWatching").empty();
        $("#birthDateWatching").empty();
        $.ajax({
            type: "POST",
            url: "/watchProfile/" + id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                updateWatchingProfile(data);
                switch (data.friendStatus) {
                    case 0:
                        $("#send").removeClass("hidden");
                        $("#send").attr("disabled", "disabled");
                        break;
                    case 1:
                        $("#accept").removeClass("hidden");
                        $("#decline").removeClass("hidden");
                        break;
                    case 3:
                        $("#send").removeClass("hidden");
                        break;
                }
            }
        });
    });

    $("#send").click(function () {
        var id = $("#watchingId").text();

        $.ajax({
            type: "POST",
            url: "/sendFriendRequest/" + id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.friendStatus === 0) {
                    $("#send").attr("disabled", "disabled");
                } else {
                    alert("Try again!");
                }
            }
        });
    });

    $("#accept").click(function () {
        var id = $("#watchingId").text();

        $.ajax({
            type: "POST",
            url: "/acceptFriendRequest/" + id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.friendStatus === 2) {
                    $("#accept").attr("disabled", "disabled");
                    $("#decline").attr("disabled", "disabled");
                } else {
                    alert("Try again!");
                }
            }
        });
    });

    $("#decline").click(function () {
        var id = $("#watchingId").text();

        $.ajax({
            type: "POST",
            url: "/declineFriendRequest/" + id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.friendStatus === 4) {
                    $("#accept").attr("disabled", "disabled");
                    $("#decline").attr("disabled", "disabled");
                } else {
                    alert("Try again!");
                }
            }
        });
    });
});

