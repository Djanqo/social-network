$(function () {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
});
$(document).ready(function () {
    $("#create-profile").click(function () {
        var name = $("#name-create").val();
        var last = $("#lastName-create").val();
        var birth = $("#birthDate-create").val();
        $.ajax({
            type: "POST",
            url: "/saveProfile",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{"name": "' + name + '", "lastName": "' + last + '", "birthDate": "' + birth + '"}',
            success: function (data) {
                window.location.href = "/contact";
            }
        });
    });
});


