INSERT INTO role VALUES (1, 'ROLE_USER');
INSERT INTO role VALUES (2, 'ROLE_ADMIN');

INSERT INTO user VALUES (1,'tester_1','$2a$11$9Q7hiAsyZcc0uSzByVYSle2gy7fYHCWIvnE.XufQekukPzGdozunO');
INSERT INTO user VALUES (2,'tester_2','$2a$11$vKz708e85ITyrlpOA67NJexTx8FdZvgk2kGFq/InA.LK0ZMaYIUSW');
INSERT INTO user VALUES (3,'tester_3','$2a$11$VQEXooxpB5ZzQqguucpIA.dqCLB7RQJmQfW3BsXS2ox793eq3sY.y');
INSERT INTO user VALUES (4,'tester_4','$2a$11$OgzwuVDRgy0sT.h.pktC.OLzQB94psvx8419YnAamOKOCHLxMZB76');
INSERT INTO user VALUES (5,'tester_5','$2a$11$jQovBGS1PMZux/ztFFl5iO4blFJA9O5iqJlbiLIHt6NZ/1vS1uoyW');

INSERT INTO user_role VALUES (1, 1);
INSERT INTO user_role VALUES (2, 1);
INSERT INTO user_role VALUES (3, 1);
INSERT INTO user_role VALUES (4, 1);
INSERT INTO user_role VALUES (5, 1);

INSERT INTO profile VALUES (1,'Darth', 'Vader', '1991-01-01');
INSERT INTO profile VALUES (2,'Luke', 'Skywalker', '1992-02-02');
INSERT INTO profile VALUES (3,'Han', 'Solo', '1993-03-03');
INSERT INTO profile VALUES (4,'Leia', 'Organa', '1994-04-04');
INSERT INTO profile VALUES (5,'Obi-Wan', 'Kenobi', '1995-05-05');

INSERT INTO relationship VALUES (1,1,5,1,1);
INSERT INTO relationship VALUES (2,2,3,1,3);
INSERT INTO relationship VALUES (3,2,5,1,5);
INSERT INTO relationship VALUES (4,3,4,1,3);
INSERT INTO relationship VALUES (5,4,5,1,5);
